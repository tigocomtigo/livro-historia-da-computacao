# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Introdução](capitulos/Introdução.md)
1. [As Primeiras Ferramentas de Cálculo](capitulos/As_primeiras_ferramentas_de_cálculo.md)
1. [Gerações de computadores](capitulos/as_gerações_de_computadores.md)
   1. [Linguagens de Programação](capitulos/Linguagens_de_programação.md)
1. [Computação móvel](capitulos/a_computação_móvel.md)
1. [Computadores Quânticos](capitulos/computação_quantica.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](imagens/avatar2.png)  | Thiago Gonçalves Salustiano | tigocomtigo | [thiagosalustiano@alunos.utfpr.edu.br](mailto:thiagosalustiano@alunos.utfpr.edu.br)
| ![](imagens/kevinperf.png) | Kevin Okuma | kdoomdm | [kdoomdm@gmail.com](mailto:doomdm@gmail.com)
