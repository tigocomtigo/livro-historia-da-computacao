# Computação quântica
## O que é?
A computação quântica é a ciência que estuda o desenvolvimento de algoritmos e softwares com base em informações que são processadas por sistemas quânticos como átomos, fótons ou partículas subatômicas, operando com as leis probabilísticas da física quântica.[1]
Na computação clássica o computador é baseado na arquitetura de Von Neumann, ou seja, possui um processador e memória destacados por um sistema de barramento de comunicação.[2]
### Empresas que estão apostando nessa tecnologia
1. IBM
2. Google
3. Intel
4. Microsoft

![Computador quântico da IBM](https://olhardigital.com.br/wp-content/uploads/2019/01/20190108060834.jpg)

## Por que a computação quântica é necessária?
Os computadores atuais estão cada vez mais perto do limite de velocidade de processamento, e isso faz com que seja extremamente difícil produzir IA's avançadas para eles. Os computadores quânticos seriam a solução para esse problema de processamento. [2]


## Como funciona um computador quântico?
A unidade usada na computação quântica é o q-bit (Bit Quântico), diferentemente do bit tradicional, pode assumir um valor de superposição entre "0" e "1", ou seja, ele pode assumir os dois valores ao mesmo tempo.
O q-bit é descrito por um vetor estados em um sistema quântico de dois níveis o qual é equivalente a um vetor de espaço bidimensional sobre números complexos[2]. Usa-se a notação de bra-ket para representá-los:
![ |0\rangle = \begin{bmatrix} 0 \\ 1\end{bmatrix} ](https://wikimedia.org/api/rest_v1/media/math/render/svg/626a1534e007d6511762add99f012fbe7db8f845)
Assim, o estado de um q-bit pode ser representado por:
![| \psi \rangle = \alpha |0 \rangle + \beta |1 \rangle,\,](https://wikimedia.org/api/rest_v1/media/math/render/svg/aabd1ffc6a57b00e254e4c212d98ebbea6ccc7fc)
Esse sistema faz com que o computador quântico seja capaz de trabalhar com uma grande quantidade de informações simultaneamente. Enquanto computadores normais demorariam milhares de anos para resolver um problema, os computadores quânticos resolveriam em alguns minutos.[3]

## O maior problema
O maior problema desses dispositivos está nos próprios q-bits. Como eles são "extremamente frágeis"(podem ser afetados por uma variação de temperatura ou uma leve vibração) existe uma maior possibilidade de erros. Apesar dessa tecnologia estar tendo bons resultados ainda estamos bem longe de um modelo de computador quântico "ideal"[4].




# Referências
[1] [O que é computação quântica? - Brasil Escola (uol.com.br)](https://brasilescola.uol.com.br/o-que-e/fisica/o-que-e-computacao-quantica.htm#:~:text=Computa%C3%A7%C3%A3o%20qu%C3%A2ntica%20%C3%A9%20a%20ci%C3%AAncia%20que%20estuda%20o,sistemas%20qu%C3%A2nticos,%20como%20%C3%A1tomos,%20f%C3%B3tons%20ou%20part%C3%ADculas%20subat%C3%B4micas.)
[2] [Computação quântica – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/Computa%C3%A7%C3%A3o_qu%C3%A2ntica)
[3] https://www.guiadacarreira.com.br/profissao/computador-quantico-ciencia-computacao/
[4] [Entenda o que são computadores quânticos e como eles funcionam - Olhar Digital](https://olhardigital.com.br/2019/09/24/noticias/entenda-o-que-sao-computadores-quanticos-e-como-eles-funcionam/)
