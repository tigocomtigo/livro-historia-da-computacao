# Introdução

   Neste livro, falaremos sobre a história da computação, citando algumas das conquistas que o ser humano conseguiu para que tivessemos um grande salto tecnológico. Dividimos este livro em vários tópicos importantes que abrangem desde a primeira ferramenta de cálculo até os computadores do futuro. Qual foi o primeiro computador? Quais são as linguagens de programação mais populares? Como funcionam os dispotivos móveis? E o que são os famosos computadores quânticos? Todas essas perguntas serão respondidas neste livro.

![](imagens/worlds-biggest-computedafafar.png)
