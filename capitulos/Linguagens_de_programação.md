# Linguagens de programação mais populares

## A primeira linguagem de programação
Ela foi inventada no ano de 1843 por Ada Lovelace, para ser usada na [máquina diferencial de Babbage](capitulos/As_primeiras_ferramentas_de_cálculo.md), ela escrevia os códigos no papel porque, como sabemos, ainda não existiam computadores naquela época e ela acabou servindo de base para todas as linguagens de programação.[1]

## Assembly
Assembly ou linguagem de montagem é uma notação legível por humanos para o código de máquina que uma arquitetura de computador específica usa, utilizada para programar códigos entendidos por dispositivos computacionais, como Microprocessadores e microcontroladores. O código de máquina torna-se legível pela substituição dos valores em bruto por símbolos chamados mnemónicos.
A tradução do código Assembly para o código de máquina é feita pelo montador ou assembler. Ele converte os mnemónicos em seus respectivos Opcodes, calcula os endereços de referências de memória e faz algumas outras operações para gerar o código de máquina que será executado pelo computador.[2]

## Fortran
O Fortran foi uma linguagem de programação de alto nível lançada inventada por John Backus para a IBM em 1954 e é utilizada até hoje nas áreas científicas e industriais, por isso é constantemente atualizada. Como o Fortran é uma linguagem de programação de alto nível é necessário que seja usado um compilador para traduzir o Fortran para código de máquina.
A sua invenção foi responsável para o desenvolvimento das industrias de software para computador e também foi utilizada para a criação de outras linguagens de alto nível.[3]

## Pascal
Criada por Niklaus Wirth em 1970, teve seu nome inspirado no matemático francês Blaise Pascal. Seu principal objetivo era de ser uma linguagem fácil de aprender. Foi favorecida pela Apple nos primeiros anos da empresa, devido sua facilidade de uso e potência. [1]

## C
O C foi criado no ano de 1972 por Denis Ritchie para ser usada no desenvolvimento do sistema operacional Unix. Ela é uma das linguagens de programação mais populares da história, por esse motivo ela acabou influenciando outras linguagens que surgiram anos depois (Java, C++, etc...).[4] Apesar de ser uma linguagem de uso geral o C é melhor utilizado em programas que lidam diretamente com hardware, como um sistema operacional (Linux) ou um driver.[5]

## C++
Desenvolvido por Bjarne Stroustrup em 1983 para ser uma adição a linguagem C o C++ é uma das linguagens mais utilizadas atualmente na programação[6]. Ele tem uma enorme variedade de códigos, pois alem de seus códigos, pode contar com vários da linguagem C. Esta variedade possibilita a programação em alto e baixo níveis.[7]

## Phyton
O Python foi lançado no início da década de 90 pelo programador e matemático holandês Guido Van Rossum. A linguagem foi projetada para dar ênfase no trabalho do desenvolvedor, facilitando a escrita de um código limpo, simples e legível, tanto em aplicações menores quanto em programas mais complexos.[8]

## Java
Java é uma linguagem de alto nível, de propósito geral, criada por James Gosling em 1995 para um projeto de TV interativa. Tem funcionalidade multi-plataforma e está consistentemente no topo das linguagens de programação mais populares do mundo. Java pode ser encontrado em qualquer lugar, desde computadores a smartphones e parquímetros. [1]

## Javascript 
Javascript foi criado no ano de 1995 por Brendan Eich. Ele permite páginas da Web interativas e, portanto, é uma parte essencial dos aplicativos da web. A grande maioria dos sites usa, e todos os principais navegadores têm um mecanismo JavaScript dedicado para executá-lo [9]. Hoje, ela é uma das mais populares do mundo, o que a faz ser considerada fundamental para programadores. O mercado tem cada vez mais se direcionado para web, o que ressalta o quanto a JavaScript ganhou papel de protagonismo.[10]

## C#
O C# (C-Sharp), é uma Linguagem de programação orientada a objetos, que foi desenvolvida pela Microsoft nos anos 2000 e faz parte da plataforma .NET. Embora a linguagem C# tenha sido criada do zero, foi baseada na linguagem C++ e tem muitos elementos da linguagem Pascal e Java.[11]





# Referências
[1] [História das linguagens de programação - DevSkiller](https://devskiller.com/pt/historia-da-programacao-idiomas/)
[2] [Linguagem assembly – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/Linguagem_assembly)
[3] [FORTRAN: História de uma linguagem de programação (oqueehistoria.com.br)](https://oqueehistoria.com.br/linguagem-de-programacao-fortran-explicada/#:~:text=O%20FORTRAN%20(ou%20tradu%C3%A7%C3%A3o%20de%20f%C3%B3rmula)%20foi%20a,usado%20hoje%20para%20programar%20aplica%C3%A7%C3%B5es%20cient%C3%ADficas%20e%20matem%C3%A1ticas.)
[4] [C (linguagem de programação) – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/C_%28linguagem_de_programa%C3%A7%C3%A3o%29)
[5] [C: a linguagem de programação que está em tudo o que você usa - Canaltech](https://canaltech.com.br/software/c-a-linguagem-de-programacao-que-esta-em-tudo-o-que-voce-usa-19512/)
[6] [C++ – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/C%2B%2B)
[7] [C++ - Linguagem de Programação - InfoEscola](https://www.infoescola.com/informatica/cpp/)
[8] [O que é Python? [Guia para iniciantes] | Aplicativos e Software | Tecnoblog](https://tecnoblog.net/405640/o-que-e-python-guia-para-iniciantes/)
[9] [JavaScript – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/JavaScript)
[10] [JavaScript: o que é, como funciona e por que usá-lo no seu site (rockcontent.com)](https://rockcontent.com/br/blog/javascript/#:~:text=JavaScript%20%C3%A9%20uma%20linguagem%20de%20programa%C3%A7%C3%A3o%20voltada%20para,que%20a%20faz%20ser%20considerada%20fundamental%20para%20programadores.)
[11] [C# - C Sharp - Linguagem de Programação - InfoEscola](https://www.infoescola.com/informatica/c-sharp/)
