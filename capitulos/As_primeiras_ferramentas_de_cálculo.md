# As primeiras ferramentas de cálculo
Com o desenvolvimento da matemática foi necessário desenvolver aparatos capazes de auxiliar na produção de cálculos, e  é nesse tópico que vai ser listado os mais famosos que já circularam pelo mundo.
## O ábaco 
O ábaco, que foi inventado na babilônia no ano de 300 A.C, tem a finalidade de contar usando as bolinhas que são dividas em duas partes, uma superior e outra inferior. A parte inferior representava as unidades e a outra contava de cinco em cinco.[1]
![A parte superior era chamada de céu](https://media.istockphoto.com/photos/abacus-picture-id91092595?k=6&m=91092595&s=612x612&w=0&h=B75-pP0O0AnUj6tC1lza1Q8DzjT4CbT8TVcwxcXhGsQ=)
## "Os ossos de Napier"
Os ossos de Napier são um dispositivo de cálculo manual criado por John Napier. O método foi baseado na multiplicação da rede, e também chamado de "rabdologia". Napier publicou sua versão em 1617, dedicada ao seu patrono Alexander Seton. Utilizando as tabelas de multiplicação embutidas nas hastes, a multiplicação pode ser reduzida a operações de adição e divisão a subtrações. O uso avançado das hastes também pode extrair raízes quadradas. Eles são baseados em tabelas de multiplicação dissecadas e podem ser usados para multiplicar e dividir números. [2]

![O dispositivo completo geralmente inclui uma placa base com uma borda; o usuário coloca as hastes dentro da borda para conduzir multiplicação ou divisão.](/imagens/68747470733a2f2f6d656469612e6973746f636b70686f746f2e636f6d2f70686f746f732f6162616375732d706963747572652d696439313039323539353f6b3d36266d3d393130393235393526733d3631327836313226773d3026683d4237352d705030.png)

### A régua de cálculo
A régua criada por William Oughtred em 1622, possui propriedades logarítmicas similares aos Ossos de Napier. Ela foi utilizada em 1960 nos programas espaciais da NASA.[3]

![](imagens/reguadecalculo.png)

## Rodas de Pascal
Criado por Blaise Pascal em 1642, as rodas de Pascal (Pascaline,= ou rodas dentadas de Pascal) era um mecanismo com vários dentes e garras capaz de realizar operações matemáticas como soma e subtração. Ela também era capaz de realizar multiplicações e divisões pela regra da soma e subtração sucessiva. Apesar de todas as suas qualidades a maquina foi um fracasso comercial, pois seu preço era extremamente elevado.[4]

![](imagens/maquinadepascal.png)

## Leibniz 
Essa foi a primeira calculadora de quatro operações (Soma, Subtração, Multiplicação e divisão). Ela foi criada em 1672 por Gottfried Wilhelm Leibniz possuindo um sistema parecido com o de Pascaline, a principal diferença era a possibilidade de executar multiplicações e divisões a partir de um mecanismo de somas e subtrações automático.[5]

![Está é a calculadora de Leibniz](imagens/leibniz.png)

## O tear de Jacquard
Criada por Joseph Marie Jacquard em 1804, o tear de Jacquard era uma máquina de tear que trançava o tecido de acordo com uma programação que era dada por um cartão que possuía furos. Sua invenção causou uma das maiores  revoluções no indústria do tecido. Para se ter uma ideia ela foi tão importante que foi declara uma propriedade publica da França.[6]

![O tear de Jacquard](imagens/otear.png)

## A máquina diferencial
Desenvolvida por Charles Babbage no ano de 1822 a máquina diferencial tinha o propósito de corrigir os erros das tabelas logarítmicas usadas pelo governo britânico. O projeto estava sendo muito demorado e custoso para o governo o que acabou causando o fim dele.[7]

[![Vídeo sobre as máquinas de Charles Babbage](imagens/maquinadiferencial.png)](https://www.youtube.com/watch?v=JtYAbLwnKfA)

## A máquina analítica
Depois do fim do projeto da máquina diferencial Charles Babbage começou a trabalhar no seu novo projeto: A máquina analítica. Ela era similar ao sistema de tear de Jacquard, pois utilizava cartões perfurados para inserir as instruções na máquina criando assim o conceito de memória.No entanto, a máquina não foi construída e apenas uma pequena parte foi montada em 1871, aquando da morte de Babbage.[8]

[![Clique para ver o vídeo sobre as máquinas de Babbage](imagens/maquinaanaliticadebabbage.png) 
](https://www.youtube.com/watch?v=s8IKAJxl75U)

# Referências 
[1.](http://producao.virtual.ufpb.br/books/camyle/introducao-a-computacao-livro/livro/livro.chunked/ch01s01.html#:~:text=Em%201642,%20o%20franc%C3%AAs%20Blaise%20Pascal,%20aos%2019,(acima),%20e%20a%20apresenta%C3%A7%C3%A3o%20da%20m%C3%A1quina%20fechada%20(abaixo).%E2%80%9D)

[2.](https://editorarealize.com.br/artigo/visualizar/26468)

[3.](https://pt.wikipedia.org/wiki/R%C3%A9gua_de_c%C3%A1lculo)

[4.](https://www.bing.com/search?q=rodas+de+pascal&qs=n&form=QBRE&sp=-1&pq=rodas+de+pascal&sc=0-15&sk=&cvid=FAC580D916F94B9AAC55D9B0A030D2FE)

[5.](https://sites.google.com/site/jamissonfatec/maquina-de-calcular-de-leibniz)

[6.](https://pt.wikipedia.org/wiki/Joseph-Marie_Jacquard)

[7.](https://historiacomp.wordpress.com/2011/07/07/maquina-diferencial-de-babbage/#:~:text=%20M%C3%A1quina%20Diferencial%20de%20Babbage%20%201%20Foi,de%20partida%20para%20a%20ind%C3%BAstria%20de...%20More)

[8.](https://www.infopedia.pt/$maquina-analitica)
