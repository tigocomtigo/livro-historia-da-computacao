# A computação móvel
## O que é computação móvel?
Computação móvel é a tecnologia que se utiliza de aparelhos pequenos e portáteis (Como celulares, pulseiras e outros) para transmitir dados através de redes sem fio.[1]

# Principais componentes e tecnologias:
## O hardware 
Os dispositivos portáteis possuem um hardware reduzido em relação aos computadores tradicionais, e além disso eles possuem vários outros aparatos integrados como câmeras, sensores de movimento, leitores de impressão digital e telas sensíveis ao toque.[2]

![Smarphone por dentro](imagens/smphonedentroreduz.png)

## Software
O desenvolvimento de software handheld(Para dispositivos móveis) abrange dispositivos como PDA's, smartphones, telefone celular, console portátil e ultra-mobile pc. Esses dispositivos geralmente possuem de fabrica tecnologias como o GPS, TV portátil, touchscreen, consoles e navegador de internet. Os aplicativos podem ser instalados na produção do aparelho ou podem ser instalados através de arquivos pela internet.[3]

![Aplicativos em um smartphone](imagens/aplicativostrat.png)

## Tecnologias mais utilizadas:
Como dito anteriormente os dispositivos são capazes de transmitir dados através da redes sem fio que incluem transferência de arquivos, interconexão de redes de longas distancias, fax, e-mail, acesso a internet e a World Wide Web. As redes sem fio utilizadas na comunicação são redes IR, Bluetooth, W-LANs, celulares, W-Packet Data e sistema de comunicação via satélite, fornecendo uma comunicação segura e confiável.[4]

![World Wide Web](imagens/WWWW.png)

# Referências
[1] [Internet móvel – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/Internet_m%C3%B3vel#:~:text=Internet%20m%C3%B3vel%20pode%20ser%20definida%20como%20o%20uso,diferentes%20da%20Internet%20tradicional,%20tipicamente%20acessada%20por%20desktops.)
[2] [O que é computação móvel? - Diferença Entre - 2021 (strephonsays.com)](https://pt.strephonsays.com/what-is-mobile-computing#:~:text=A%20computa%C3%A7%C3%A3o%20m%C3%B3vel%20%C3%A9%20a%20tecnologia%20usada%20para,meio%20de%20uma%20rede%20sem%20fio%20para%20comunica%C3%A7%C3%A3o.)
[3] [Desenvolvimento de software móvel – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/Desenvolvimento_de_software_m%C3%B3vel)
[4] [O que é computação móvel? - Diferença Entre - 2021 (strephonsays.com)](https://pt.strephonsays.com/what-is-mobile-computing#:~:text=A%20computa%C3%A7%C3%A3o%20m%C3%B3vel%20%C3%A9%20a%20tecnologia%20usada%20para,meio%20de%20uma%20rede%20sem%20fio%20para%20comunica%C3%A7%C3%A3o.)
