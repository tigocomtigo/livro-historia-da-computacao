## A primeira geração de computadores
A primeira geração de computadores foi criada entre os anos de 1946 até 1959. Eles usavam os cartões perfurados, fita de papel ou fita magnética como dispositivo de entrada e saída. Por serem caros, grandes, pesados e possuírem sérios problemas de refrigeração eles foram adotados apenas por grandes corporações[1]. Alguns exemplos de computadores que foram desenvolvidos nessa época: 
1. ENIAC
2. EDVAC
3. UNIVAC
4. IBM-650
5. IBM-701

![Este foi o ENIAC um dos computadores da primeira geração](https://meteoropole.com.br/site/wp-content/uploads/2017/05/shutterstock_339962852.jpg)

## A segunda geração (1959-1964)
A introdução da segunda geração de computadores foi marcada pela substituição das válvulas eletrônicas por transistores e pela tecnologia de circuitos impressos o que causou uma diminuição considerável no tamanho deles.  O surgimento dos computadores da segunda geração também fez com que várias linguagens de programação fossem desenvolvidas (Fortran, Cobol e Algol), isso proporcionou uma maior facilidade na criação de software[2]. Alguns computadores dessa época foram:
1. IBM 7030
2. PDP-8 (Este era uma versão mais básica e menor dos computadores, mas ainda assim era bem espaçoso)

![](https://c1.staticflickr.com/7/6111/6236080776_d27d9a8266_b.jpg)

## A terceira geração (1965-1971)
Naquele tempo houve a substituição dos transistores por circuitos integrados (IC's). Inventado por Jack Kilby, os IC's possuem vários transistores, resistores e capacitores. Isso fez com que os computadores tivessem seu tamanho ainda mais reduzido, se tornassem confiáveis eficientes. Esta geração de computadores também foi responsável pela criação de sistemas operacionais e desenvolvimento de linguagens de alto nível como: FORTRAN-II TO IV, COBOL, PASCAL PL/1, BASIC, ALGOL-68[3]. Os principais representantes foram:
1. IBM-360 series
3. PDP (Programmed Data Processor)
4. TDC 316
![enter image description here](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/PDP-1.jpg/1200px-PDP-1.jpg)
## A quarta geração (1971 -1983)
A quarta geração foi a responsável pela introdução dos microprocessadores, mais conhecidos como CPU (Central Processing Unit). No início da dos anos 70 os CPU's conseguiam processar cerca de 100.000 informações por segundo. Também é de grande importância destacar que a partir da quarta geração os computadores se tornaram mais acessíveis, e além disso a internet teve seu início[4]. Os principais modelos desse período foram:
1. O Apple II
2. Power PC
3. Altair 8800
![Este é o Apple II](imagens/appleii.png)

## Quinta geração (1983 - os dias atuais)
O nome de quinta geração vem de um projeto japonês na década de 70 que tinha como objetivo desenvolver um computador capaz de usar tecnologias de inteligência artificial e softwares avançados. A geração atual abrange não apenas a inteligência artificial, mas também os sistemas especializados, nanotecnologia, computadores quânticos e a robótica. Houve também uma redução ainda maior no tamanho do hardware permitindo que dispositivos ultrafinos começassem a ser produzidos. A quinta geração também foi a responsável pela maior acessibilidade, portabilidade e diversidade vista até então.[5]
![O Iphone é um dos smartphones mais populares do planeta](https://www.notebookcheck.com/fileadmin/Notebooks/Apple/iPhone_6/teaser_main.jpg)

# Referências
[1] [Computador - Primeira geração - Tutorialspoint](https://www.tutorialspoint.com/pg/computer_fundamentals/computer_first_generation.htm#:~:text=Computador%20-%20Primeira%20gera%C3%A7%C3%A3o.%20O%20per%C3%ADodo%20da%20primeira,circuitos%20para%20a%20CPU%20(Unidade%20Central%20de%20Processamento).)
[2] [Computação moderna – Segunda Geração (1959 – 1964)! | A História dos Computadores (wordpress.com)](https://hestoriadopc.wordpress.com/2011/07/04/computacao-moderna-segunda-geracao-1959-1964/)
[3] [Computador - Terceira geração - Tutorialspoint](https://www.tutorialspoint.com/pg/computer_fundamentals/computer_third_generation.htm)
[4] [Quarta geração de computadores – Wikipédia, a enciclopédia livre (wikipedia.org)](https://pt.wikipedia.org/wiki/Quarta_gera%C3%A7%C3%A3o_de_computadores#Hist%C3%B3ria)
[5] [O que é a quinta geração de computadores? - Maestrovirtuale.com](https://maestrovirtuale.com/o-que-e-a-quinta-geracao-de-computadores/#:~:text=A%20quinta%20gera%C3%A7%C3%A3o%20de%20computadores%20%C3%A9%20como%20designa,r%C3%A1pidos%20e%20eficientes%20e%20possuem%20software%20moderno%20desenvolvido.)
